#ifndef _CIRCLE_H_
#define _CIRCLE_H_

class Circle
{
private:
	double Radius;
	double Ference;
	double Area;

public:
	Circle() 
	{
		Radius = 0;
		Ference = 0;
		Area = 0;
	}
	void setRadius(double Rad);
	void setFerence(double Fer);
	void setArea(double Ar);
	double getRadius() const;
	double getFerence() const;
	double getArea() const;
};

#endif