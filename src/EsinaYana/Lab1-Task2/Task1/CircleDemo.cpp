#include "Circle.h"
#include <iostream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");
	cout << "Task \"Earth and Rope\":" << endl<<endl;
	
	Circle Earth;
	Circle EarthPlusRope;
	double Zazor;
	double const RADIUS_EARTH = 6378.1;

	Earth.setRadius(RADIUS_EARTH);
	EarthPlusRope.setFerence(Earth.getFerence() + 1);
	Zazor = EarthPlusRope.getRadius() - Earth.getRadius();

	cout << "Radius Earth " << Earth.getRadius()<<" km." << endl;
	cout << "Radius Earth+1 " << EarthPlusRope.getRadius() << " km." << endl;
	cout << "Area Earth " << Earth.getArea() << " km." << endl;
	cout << "Area Earth +1 " << EarthPlusRope.getArea() << " km." << endl;
	cout << "Ference Earth " << Earth.getFerence() << " km." << endl;
	cout << "Ference Earth+1 " << EarthPlusRope.getFerence() << " km." << endl << endl;
	
	cout << "Gap = " << Zazor << " km." <<endl<<endl<<endl;

	cout << "Task \"Pool\":" << endl<<endl;
	
	Circle Pool;
	Circle Track;
	double Value;
	int const TRACK_WIDHT = 1;
	int const POOL_RADIUS = 3;
	int const VALUE_TRACK = 1000;
	int const VALUE_FENCE = 2000;
	
	Pool.setRadius(POOL_RADIUS);
	Track.setRadius(Pool.getRadius() + TRACK_WIDHT);
	Value = (Track.getArea() - Pool.getArea())*VALUE_TRACK*TRACK_WIDHT + Track.getFerence()*VALUE_FENCE;
	cout << "Value: " << Value<< " rubles."<<endl;

	system("pause");
	return 0;
}