#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QLCDNumber>
#include <QLabel>
#include <QProgressBar>
#include <QString>
#include "Automat.h"
#include <QMap>
#include <QtGui>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT


public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private:
    Ui::MainWindow *ui;
Automat drink;
//***********************************************
int Money;
int menu_size;
QTimer *qt;
int tval;
QString temp;
//***********************************************
    QGridLayout *layout = new QGridLayout;
    QHBoxLayout *layout1 = new QHBoxLayout;
    QHBoxLayout *layout2 = new QHBoxLayout;
    QVBoxLayout *layout2_1 = new QVBoxLayout;
    QVBoxLayout *layout2_2 = new QVBoxLayout;
    QVBoxLayout *layout2_3 = new QVBoxLayout;
    QVBoxLayout *layout2_4 = new QVBoxLayout;
    QVBoxLayout *layout3 = new QVBoxLayout;
    QVBoxLayout *layout4 = new QVBoxLayout;
    QVBoxLayout *layout5 = new QVBoxLayout;
    QHBoxLayout *layout6 = new QHBoxLayout;

    QLabel *Label_Cook_1  = new QLabel(".................");
    QProgressBar* Bar_Cook_1 =new QProgressBar();

    QLabel *Label_Price_2  = new QLabel("Стоимость напитка:");
    QLCDNumber *LCD_Price_2  = new QLCDNumber();

    QLabel *Label_Account_2  = new QLabel("Всего на счету:   ");
    QLCDNumber *LCD_Account_2  = new QLCDNumber();

    QLCDNumber *LCD_Money_2  = new QLCDNumber();

    QLabel *Label_Change_6  = new QLabel("Ваша сдача:");
    QLCDNumber *LCD_Change_6  = new QLCDNumber();

    QPushButton *b_ON_2 = new QPushButton("ВКЛ");
    QPushButton *b_OFF_2 = new QPushButton("ВЫКЛ");

    QPushButton *b_Cook_1 = new QPushButton("Приготовить");

    QPushButton *b_Money_2 = new QPushButton("Бросить");
    QPushButton *b_Accept_2 = new QPushButton("Внести \nна счет");
    QPushButton *b_Cancel_2 = new QPushButton("Сброс");

   QVector<QPushButton* > b_menu;
   QPushButton *b_Change_6 = new QPushButton("Забрать");

   QVector <QLabel * > l_menu;
   QVector <QLabel * > l2_menu;
//***********************************************
private slots:
void art_windows(); // рисуем автомат
void art_cancel();  // вкл/выкл кнопок по cancel
void start();
void stop();
void cancel();
void coin();   //Имитация внесения денег
void Accept(); //положить деньги на счет
void menu();   //меню
void cook();   //приготовление напитка
void timeout();
void endError();
//***********************************************
};

#endif // MAINWINDOW_H
