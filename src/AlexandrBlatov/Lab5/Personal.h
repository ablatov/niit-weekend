#include <iostream>
#include "I_Salary.h"
#include "Staff.h"


#ifndef _PERSONAL_H
#define _PERSONAL_H

class Personal :public IWorkTime, public Employee	//	�������� �� ����� � ������� �� ���������� ������������ �����.����� ������ �� ���.
{
public:
	Personal(){}
	int calcWorkTime(int worktime, int rate)
	{
		return worktime * rate;
	}
	virtual void CalcPayment() = 0;//(int rate = 0, int over = 0) ���� ������������� ������� ������
	~Personal()	{}
private:
	Personal& operator= (Personal&);
	Personal(Personal&);
};

class Cleaner :  public Personal //- ��������
{
private:
	unsigned Rate = 40;
public:
	Cleaner(){};
	Cleaner(string post, string Sname, string name, string Mname, int id = NULL)
	{
		Post = post;
		Surname = Sname;
		Name = name;
		Midname = Mname;
		if (id != NULL)
		{
			ID = id;
			TotalID++;
		}
		else
		{
		    TotalID++;
		    ID = TotalID;
		}
	}

	void setWorkTime(int worktime)
	{
		Worktime = worktime;
	}

	void CalcPayment()
	{
		Payment = calcWorkTime(Worktime, Rate);
	}

	int  getPayment() const
	{
		return Payment;
	}
	
	int getID() const
	{

		return ID;
	}
	
	string getFOI() const
	{
		return Surname + " " + Name + " " + Midname;
	}
	
	string getPost() const
	{
		return Post;
	}

	~Cleaner()	{}
private:
	Cleaner& operator= (Cleaner&);
	Cleaner(Cleaner&);
};

class Driver : public Personal //��������.
{
private:
	unsigned Rate = 50;
	unsigned RateNightTime = 70;
	int Overtime;
public:
	Driver() {};
	Driver(string post, string Sname, string name, string Mname, int id = NULL)
	{
		Post = post;
		Surname = Sname;
		Name = name;
		Midname = Mname;
		if (id != NULL)
		{
			ID = id;
			TotalID++;
		}
		else
		{
			TotalID++;
			ID = TotalID;
		}
	}

	void setWorkTime(int worktime)
	{
		Worktime = worktime;
	}

	void setOverTime(int nighttime)
	{
		Overtime = nighttime;
	}

	void CalcPayment()
	{
		Payment = calcWorkTime(Worktime, Rate) + calcWorkTime(Overtime, RateNightTime);
	}

	int  getPayment() const
	{
		return Payment;
	}
	int getID() const
	{
		  
		return ID;
	}
	string getFOI() const
	{
		return Surname + " " + Name + " " + Midname;
	}
	string getPost() const
	{
		return Post;
	}
	~Driver(){}

private:
	Driver& operator= (Driver&);
	Driver(Driver&);
};

#endif
