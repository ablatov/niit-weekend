#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <Windows.h>
#include <iomanip> 
#include "Staff.h"
#include "Personal.h"
#include "Engineer.h"
#include "Manager.h"

using namespace std;

static int strTOint(string str)
{
	return atoi(str.c_str());
}

static Employee* FillingEmp(Employee* ptrEmp, vector <string> data)
{

	int id = strTOint(data.at(1));

	if (dynamic_cast <Cleaner*> (ptrEmp))
	{
		ptrEmp = new Cleaner(data.at(0), data.at(2), data.at(3), data.at(4), id);
	}
	else if (dynamic_cast <Driver*> (ptrEmp))
	{
		ptrEmp = new Driver(data.at(0), data.at(2), data.at(3), data.at(4), id);
	}
	else if (dynamic_cast <Programmer*> (ptrEmp))
	{
		if (dynamic_cast <TeamLeader*> ((Programmer*)ptrEmp))
		{
			ptrEmp = new TeamLeader(data.at(0), data.at(2), data.at(3), data.at(4), id);
		}
		else
		{
			ptrEmp = new Programmer(data.at(0), data.at(2), data.at(3), data.at(4), id);
		}
	}
	else if (dynamic_cast <Tester*> (ptrEmp))
	{
		ptrEmp = new Tester(data.at(0), data.at(2), data.at(3), data.at(4), id);
	}
	else if (dynamic_cast <Manager*> (ptrEmp))
	{
		if (dynamic_cast <ProjectManager*> ((Manager*)ptrEmp))
		{
			if (dynamic_cast <SeniorManager*> ((ProjectManager*)ptrEmp))
			{
				ptrEmp = new SeniorManager(data.at(0), data.at(2), data.at(3), data.at(4), id);
			}
			else
			{
				ptrEmp = new ProjectManager(data.at(0), data.at(2), data.at(3), data.at(4), id);
			}
		}
		else
		{
			ptrEmp = new Manager(data.at(0), data.at(2), data.at(3), data.at(4), id);
		}
	}
	else
	{
		return nullptr;
	}

	return ptrEmp;
}

static void FullWorkTime(Employee* ptrEmp, vector <string> data, vector<int> project)
{
	int t_work = strTOint(data.at(5));

	if (dynamic_cast <Cleaner*> (ptrEmp))
	{
		((Cleaner*)ptrEmp)->setWorkTime(t_work);
	}
	else if (dynamic_cast <Driver*> (ptrEmp))
	{
		((Driver*)ptrEmp)->setWorkTime(t_work);

		int o_work = strTOint(data.at(6));
		((Driver*)ptrEmp)->setOverTime(o_work);
	}
	else if (dynamic_cast <Programmer*> (ptrEmp))
	{
		if (dynamic_cast <TeamLeader*> ((Programmer*)ptrEmp))
		{
			((TeamLeader*)ptrEmp)->setWorkTime(t_work);
			int num = strTOint(data.at(7));
			((TeamLeader*)ptrEmp)->setProject(project.at(num));
			int p_head = strTOint(data.at(6));
			((TeamLeader*)ptrEmp)->setNumHeading(p_head);
		}
		else
		{
			((Programmer*)ptrEmp)->setWorkTime(t_work);
			int o_work = strTOint(data.at(6));
			((Programmer*)ptrEmp)->setOverTime(o_work);
			int num = strTOint(data.at(7));
			((Programmer*)ptrEmp)->setProject(project.at(num));
		}
	}
	else if (dynamic_cast <Tester*> (ptrEmp))
	{
		((Tester*)ptrEmp)->setWorkTime(t_work);
		int o_work = strTOint(data.at(6));
		((Tester*)ptrEmp)->setOverTime(o_work);
		int num = strTOint(data.at(7));
		((Tester*)ptrEmp)->setProject(project.at(num));
	}
	else if (dynamic_cast <Manager*> (ptrEmp))
	{
		if (dynamic_cast <ProjectManager*> ((Manager*)ptrEmp))
		{
			if (dynamic_cast <SeniorManager*> ((ProjectManager*)ptrEmp))
			{
				((SeniorManager*)ptrEmp)->setWorkTime(t_work);

				int p_head = strTOint(data.at(6));
				((SeniorManager*)ptrEmp)->setNumHeading(p_head);
				for (int price : project)
				{
					((SeniorManager*)ptrEmp)->setProject(price);
				}
			}
			else
			{
				((ProjectManager*)ptrEmp)->setWorkTime(t_work);
				int num = strTOint(data.at(7));
				((ProjectManager*)ptrEmp)->setProject(project.at(num));
				int p_head = strTOint(data.at(6));
				((ProjectManager*)ptrEmp)->setNumHeading(p_head);
			}
		}
		else
		{
			((Manager*)ptrEmp)->setWorkTime(t_work);
			int num = strTOint(data.at(6));
			((Manager*)ptrEmp)->setProject(project.at(num));
		}
	}

}

/* //*****************��� ���������� ������� �� ����� �������**********************
static void Payment(Employee* ptrEmp)
{
if (dynamic_cast <Cleaner*> (ptrEmp))
{
((Cleaner*)ptrEmp)->CalcPayment();
}
else if (dynamic_cast <Driver*> (ptrEmp))
{
((Driver*)ptrEmp)->CalcPayment();
}
else if (dynamic_cast <Programmer*> (ptrEmp))
{
if (dynamic_cast <TeamLeader*> ((Programmer*)ptrEmp))
{
((TeamLeader*)ptrEmp)->CalcPayment();
}
else
{
((Programmer*)ptrEmp)->CalcPayment();
}
}
else if (dynamic_cast <Tester*> (ptrEmp))
{
((Tester*)ptrEmp)->CalcPayment();
}
}
*/