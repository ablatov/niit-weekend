#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <sstream>
#include <Windows.h>
#include <iomanip> 
#include "Staff.h"
#include "Personal.h"
#include "Engineer.h"
#include "Manager.h"
#include "Foo.cpp"

using namespace std;
int Employee::TotalID = 0; 

int main()
{
	SetConsoleCP(1251);// ��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); // ��������� ������� �������� win-cp 1251 � ����� ������

	vector <Employee *> staff; //������ ��� �������� �����������
	Employee* ptr;
	vector<int> project = { 500, 1000, 700, 10000, 300 }; //������ � ������ ��������
	
	map <string, Employee*> Emp = { { "Cleaner", new Cleaner },
	                                { "Driver", new Driver },	                               
									{ "Programmer", new Programmer },
									{ "TeamLeader", new TeamLeader },
                                    { "Tester", new Tester },
									{ "Manager", new Manager },
									{ "ProjectManager", new ProjectManager },
									{ "SeniorManager", new SeniorManager }
	                               };
		
	ifstream fstaff("Base.txt");
	if (fstaff.fail())
		cerr << "File Base.txt error" << endl;
	
	vector <string> post; // 2 �������� �-��� FillingEmp
	while (1)
	{
    if (fstaff.eof())
		break;
	
	string val,val1;
	getline(fstaff, val, '\n'); //����� ������
	istringstream ist(val);
	while (getline(ist, val1, ';')) //����� �����
	    {
		post.push_back(val1); 
	    }
	ptr = Emp.at(post.at(0));  // 1 �������� �-��� FillingEmp
	ptr = FillingEmp(ptr, post);
	FullWorkTime(ptr, post, project);
    staff.push_back(ptr);
	post.clear();

}
	fstaff.close();

	for (auto it = 0; it < staff.size(); ++it)
	{
	//	Payment(staff[it]); //��������� �������� ����� �������
		staff[it]->CalcPayment();
		cout << setfill(' ') << setw(15) << left << staff[it]->getPost() << "ID - ";
		cout << setfill(' ') << setw(3) << left << staff[it]->getID() << "Name: ";
		cout << setfill(' ') << setw(33) << left << staff[it]->getFOI() << "Payment: ";
		cout<<staff[it]->getPayment() << endl;
	}
	
	return 0;
}