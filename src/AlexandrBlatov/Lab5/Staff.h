#include <string>
#include <iostream>
#include "I_Salary.h"
using namespace std;

#ifndef _STAFF_H
#define _STAFF_H

static int TotalID = 0;
class Employee   //��������.�������� ������������ ����� ��� ���� �������������� ����������.
{

protected:
	static int TotalID;
	int ID;            //����������������� �����.
	string Surname;    //�������
	string Name;       //���
	string Midname;    //�������
	string Post;       //���������
	int	Worktime;      //������������ �����.
	int	Payment; //���������� �����.
public:
	Employee(){	}
	virtual void setWorkTime(int) = 0;
	virtual  int  getPayment() const = 0;
	virtual int getID() const = 0;
	virtual string getFOI() const = 0;
	virtual string getPost() const = 0;
	virtual void CalcPayment() = 0;
	~Employee()	{}
};

#endif 