#include <iostream>
#include <string>
#include <vector>
#include "I_Salary.h"
#include "Staff.h"

using namespace std;

#ifndef _MANAGER_H
#define _MANAGER_H

class Manager : public Employee, public IProject//- ��������.������ �������� �� ����� �������, ������� ���������.
{
protected:
	int Price;
	int Percent = 5;          // % ������� � �������
public:

	Manager(){}
	Manager(string post, string Sname, string name, string Mname, int id = NULL)
	{
		Post = post;
		Surname = Sname;
		Name = name;
		Midname = Mname;
		if (id != NULL)
		{
			ID = id;
			TotalID++;
		}
		else
		{
			TotalID++;
			ID = TotalID;
		}
	}

	void setWorkTime(int worktime)
	{
		Worktime = worktime;
	}

	void setProject(int price)
	{
		Price = price;
	}

	virtual int calcRatioProject(int price, int percent)
	{
		return price * percent / 100;
	}

	void CalcPayment()
	{
		Payment = calcRatioProject(Price, Percent);
	}

	int  getPayment() const
	{
		return Payment;
	}

	int getID() const
	{

		return ID;
	}

	string getFOI() const
	{
		return Surname + " " + Name + " " + Midname;
	}

	string getPost() const
	{
		return Post;
	}

	~Manager(){}
private:
	Manager& operator= (Manager&);
	Manager(Manager&);
};

class ProjectManager:public Manager, public IHeading // ��������� ��������.
{
protected:
	int Head;
	int RateHead = 500;
public:

	ProjectManager(){}
	ProjectManager(string post, string Sname, string name, string Mname, int id = NULL)
	{
		Post = post;
		Surname = Sname;
		Name = name;
		Midname = Mname;
		if (id != NULL)
		{
			ID = id;
			TotalID++;
		}
		else
		{
			TotalID++;
			ID = TotalID;
		}
	}
	void setWorkTime(int worktime)
	{
		Worktime = worktime;
	}

	void setProject(int price)
	{
		Price = price;
	}
	void setNumHeading(int head = 0)
	{
		Head = head;
	}
	int calcNumHeading(int head, int ratehead)
	{
		return head * ratehead;
	}

	void CalcPayment()
	{
		Payment = calcRatioProject(Price, Percent) + calcNumHeading(Head, RateHead);
	}

	int  getPayment() const
	{
		return Payment;
	}

	int getID() const
	{

		return ID;
	}

	string getFOI() const
	{
		return Surname + " " + Name + " " + Midname;
	}

	string getPost() const
	{
		return Post;
	}

	~ProjectManager(){}
private:
	ProjectManager& operator= (ProjectManager&);
	ProjectManager(ProjectManager&);
};
class SeniorManager: public ProjectManager  // ������������ �����������.
{
	vector <int> PriceProject;
	int Percent = 4;
	int Head;
public:

	SeniorManager(){}
	SeniorManager(string post, string Sname, string name, string Mname, int id = NULL)
	{
		Post = post;
		Surname = Sname;
		Name = name;
		Midname = Mname;
		if (id != NULL)
		{
			ID = id;
			TotalID++;
		}
		else
		{
			TotalID++;
			ID = TotalID;
		}
	}
	void setWorkTime(int worktime)
	{
		Worktime = worktime;
	}

	void setProject(int price)
	{
		PriceProject.push_back(price);
	}
	void setNumHeading(int head = 0)
	{
		 Head = head;
		
	}
	int calcNumHeading(int head, int ratehead)
	{
		return head * ratehead;
	}

	void CalcPayment()
	{
		Payment = calcNumHeading(Head, RateHead);
		for (int i = 0; i < PriceProject.size(); ++i)
		{
			Payment += calcRatioProject(PriceProject.at(i), Percent);
		}
	}

	int  getPayment() const
	{
		return Payment;
	}

	int getID() const
	{

		return ID;
	}

	string getFOI() const
	{
		return Surname + " " + Name + " " + Midname;
	}

	string getPost() const
	{
		return Post;
	}

	~SeniorManager(){}
private:
	SeniorManager& operator= (SeniorManager&);
	SeniorManager(SeniorManager&);
};

#endif