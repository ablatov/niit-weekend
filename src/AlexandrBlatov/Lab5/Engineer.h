#include <iostream>
#include <string>
#include "I_Salary.h"
#include "Staff.h"

using namespace std;

#ifndef _ENGINEER_H
#define _ENGINEER_H

class Engeneer :public IWorkTime, public IProject, public Employee	//	�������. ����� ������ � ������ �� ��� + ������ �� ������������ �������.
{
public:
	Engeneer(){}
	int calcWorkTime(int worktime, int rate)
	{
		return worktime * rate;
	}
	int calcRatioProject(int price, int percent)
	{
		return price * percent/100;
	}
	virtual void CalcPayment() = 0;

	~Engeneer()	{}
private:
	Engeneer& operator= (Engeneer&);
	Engeneer(Engeneer&);
};

class Programmer : public Engeneer //- �������-�����������
{
protected:
	unsigned Rate = 60;
	unsigned Percent = 2;
	int Price;
	unsigned RateNightTime = 50;
	int Overtime;

public:
	Programmer(){}
	Programmer(string post, string Sname, string name, string Mname, int id = NULL)
	{
		Post = post;
		Surname = Sname;
		Name = name;
		Midname = Mname;
		if (id != NULL)
		{
			ID = id;
			TotalID++;
		}
		else
		{
			TotalID++;
			ID = TotalID;
		}
	}

	void setWorkTime(int worktime)
	{
		Worktime = worktime;
	}

	void setOverTime(int nighttime)
	{
		Overtime = nighttime;
	}

	void setProject(int price)
	{
		Price = price;
	}

	virtual void CalcPayment()
	{
		Payment = calcWorkTime(Worktime, Rate) + calcWorkTime(Overtime, RateNightTime)+ calcRatioProject(Price, Percent);
	}

	int  getPayment() const
	{
		return Payment;
	}

	int getID() const
	{
		return ID;
	}

	string getFOI() const
	{
		return Surname + " " + Name + " " + Midname;
	}

	string getPost() const
	{
		return Post;
	}

	~Programmer()	{}
	
private:
	
	Programmer& operator= (Programmer&);
	Programmer(Programmer&);

};
class Tester : public Engeneer //������� �� ������������
{
private:
	unsigned Rate = 50;
	unsigned Percent = 1;
	int Price;
	unsigned RateNightTime = 70;
	int Overtime;
public:
	Tester(){}
	Tester(string post, string Sname, string name, string Mname, int id = NULL)
	{
		Post = post;
		Surname = Sname;
		Name = name;
		Midname = Mname;
		if (id != NULL)
		{
			ID = id;
			TotalID++;
		}
		else
		{
			TotalID++;
			ID = TotalID;
		}
	}

	void setWorkTime(int worktime)
	{
		Worktime = worktime;
	}

	void setOverTime(int nighttime)
	{
		Overtime = nighttime;
	}

	void setProject(int price)
	{
		Price = price;
	}

	void CalcPayment()
	{
		Payment = calcWorkTime(Worktime, Rate) + calcWorkTime(Overtime, RateNightTime) + calcRatioProject(Price, Percent);
	}

	int  getPayment() const
	{
		return Payment;
	}

	int getID() const
	{
		return ID;
	}

	string getFOI() const
	{
		return Surname + " " + Name + " " + Midname;
	}

	string getPost() const
	{
		return Post;
	}

	~Tester(){}
private:
	Tester& operator= (Tester&);
	Tester(Tester&);

};

class TeamLeader : public Programmer, public IHeading //������� �����������.
	{
	private:
		int Head;
		int RateHead=500;
	public:
		TeamLeader(){}
		TeamLeader(string post, string Sname, string name, string Mname, int id = NULL)
	{
		Post = post;
		Surname = Sname;
		Name = name;
		Midname = Mname;
		if (id != NULL)
		{
			ID = id;
			TotalID++;
		}
		else
		{
			TotalID++;
			ID = TotalID;
		}
	}

		void setWorkTime(int worktime)
		{
			Worktime = worktime;
		}

		void setProject(int price)
		{
			Price = price;
		}

		void setNumHeading(int head = 0)
			{
				Head = head;
			}

		int calcNumHeading(int head, int ratehead)
		{
			return head * ratehead;
		}
		
		void CalcPayment() 
		{
			Payment = calcWorkTime(Worktime, Rate)  + calcRatioProject(Price, Percent) + calcNumHeading(Head, RateHead); 
		}

		int  getPayment() const
		{
			return Payment;
		}

		int getID() const
		{
			return ID;
		}

		string getFOI() const
		{
			return Surname + " " + Name + " " + Midname;
		}

		string getPost() const
		{
			return Post;
		}

		~TeamLeader() {}

	private:
		TeamLeader& operator= (TeamLeader&);
		TeamLeader(TeamLeader&);
};

#endif