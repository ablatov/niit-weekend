#include <string>
#include <iostream>
#include <vector>
#include <iterator>
#ifndef _DEKANAT_H
#define _DEKANAT_H
using namespace std;

enum STATISTICS { STAT_AV_GROUP, STAT_AV_STUDENT };
enum CONSOLE {	LIST_NUM_GROUP, LIST_GROUP, ADD_MARK_STUDENT, 
				LIST_STUDENT, STUD_TRANSFER, STUD_EXPELLED, LIST_EXPELLED };

class Group;
//**************************************************************************************************//
class Student 
{
private:
	static int TotalID;
	int ID;				//����������������� �����
	string	Fio;		//������� � ��������
	const Group *group;		//������ �� ������(������ Group)
	int* Marks;			//������ ������
	int Num;		//���������� ������

		friend class Group; 
		friend class Dekanat;

	Student(string fio)
	{
		TotalID++;
		ID = TotalID;
		Fio = fio; 
		this->group = nullptr; 
		Marks = 0; 
		Num = 0;
	}

	~Student()
	{
		TotalID--;
	}
	void setStudent(string	fio); //�������� �������� � ��������� �� � ���, �������������� ��������
	void setGroup(Group *group); //	���������� � ������ (��������� ������) 
	void setMarks(int *marks, int num);//	���������� ������
	void setMarks(int mark);//	���������� ������
	int getAvMarkStud() const;	//	���������� ������� ������

	Student& operator = (Student&); //������� (������) ������������
	Student(Student&);				//������� (������) �����������
};

//**************************************************************************************************//
class Group 
{
	private:
	string Title;				//�������� ������
	vector<Student*> Students;	//������ �� ������ �� ��������� 
	int Num;					//���������� ��������� � ������
	Student* Head;				//������ �� ��������(�� ������ ������)  
	
		friend class Dekanat;
		Group(string title = NULL)			//����������� ������, � ��������� ��������
		{
			Title = title;
			vector<Student*> Students;
			Num = 0;
			Head = 0;
		}
		~Group()
		{}
		
		void setGroup(string title);				// ��������/�������������� ������ � ��������� ��������
		string getTitle()const;						// ������ ������ ������
		string getHead() const;						// ������ ��� ��������	
		int getAvMarkGroup() const;					// ���������� �������� ����� � ������
		string getlistGroup() const;				// ������ ������ ��������� ������
		void addStudent(Student &stud);				// ���������� ��������
		void appHead(string fio);					// �������� �������� �� ���
		void appHead(int id);						// �������� �������� �� ID
		Student* searchStud(string fio);			// ����� �������� �� ��� ��� ��
		Student* searchStud(int id);				// ����� �������� �� ��� ��� ��
		void delStudToGroup(Student &stud);			// ���������� �������� �� ������

		Group& operator = (Group&); //������� (������) ������������
		Group( Group&);				//������� (������) �����������
};
//**************************************************************************************************//
class Dekanat
{
private:
	vector<Group*> Groups;
	vector<Student*> Heads;
	int Stat;
	int Console;
	static Dekanat* Singl;
	static int Count;
	Dekanat() 
	{
		vector<Group*> Groups;
		vector<Student*> Heads;
	}
~Dekanat(){}
public:
	static Dekanat* Create(void);					//�������� ��������
	void delDekanat();								//�������� ��������

	void setFileRead();								//�������� ����� �� ������ ������ �� �����
	void setFileRead(string groupName);				//�������� ��������� �� ������ ������ �� �����
	void getFileWrite() const;						//���������� ����������� ������ � ������ 
	void rndMarks(Student &stud) const;				//���������� ��������� ������ ���������
	Group* searchGroup(string groupName) const;		//����� ������
	Student* searchStud(Group* ptrGroup) const;		//����� �������� � ������
	void ExpellStudent() const;						//���������� ��������� �� ��������������
	void electHeads() const;						//��������� ������� ������� � �������
	void transferStudent(Student &stud, Group &oldgroup, Group &newgroup) const;//������� ��������� �� ������ � ������
	string getStat(STATISTICS Stat) const;			//���������� ���������� �� ������������ ��������� � �����
	string getConsole(CONSOLE Console) const;		//����� ������ �� �������
};

#endif