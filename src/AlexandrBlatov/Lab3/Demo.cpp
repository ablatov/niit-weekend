#define _CRT_SECURE_NO_WARNINGS
#include "VLInt.h"
#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");
	try
	{
		//	string a = "876532897456345873985456983745"; //��� �������� ��������� string

		char a[100];
		cin >> a;
		int b = strlen(a);
		VLInt First;
		First.setNumber(a);
		cout << "�������� 1 - " << First.getNumber() << endl;
		VLInt Two;
		Two = First;
		cout << "�������� 2 - " << Two.getNumber() << endl;
		//	int c [20] = {1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0}; //��� �������� � �������� int
		char c[100];
		cin >> c;
		int d = strlen(c);
		VLInt Three;
		Three.setNumber(c);
		//	Three.setNumber(c, 20);									//��� �������� � �������� int
		//	//VLInt Three(20);
		Two = Three;
		//********************************	��������	*****************
		cout << '\n' << Three.getNumber() << " + " << First.getNumber() << endl;
		Three = Three + First;
		cout << "�������� 3 (+) - " << Three.getNumber() << endl;

		cout << '\n' << Two.getNumber() << " - " << First.getNumber() << endl;
		Three = Two - First;
		cout << "�������� 4 (-) - " << Three.getNumber() << endl;

		cout << '\n' << Three.getNumber() << " += " << First.getNumber() << endl;
		Three += First;
		cout << "�������� 5 (+=) - " << Three.getNumber() << endl;

		cout << '\n' << First.getNumber() << " -= " << Three.getNumber() << endl;
		First -= Three;
		cout << "�������� 6 (-=) - " << First.getNumber() << endl;

		cout << '\n' << Three.getNumber() << " * " << Two.getNumber() << endl;
		First = Three * Two;
		cout << "�������� 7 (*) - " << First.getNumber() << endl;

		cout << '\n' << First.getNumber() << " / " << Two.getNumber() << endl;
		Three = First / Two;
		cout << "�������� 8 (/) - " << Three.getNumber() << endl;

		VLInt Four;

		cout << '\n' << First.getNumber() << " % " << Two.getNumber() << endl;
		Four = First % Two;
		cout << "�������� 9 (%) - " << Four.getNumber() << endl;

		cout << '\n' << First.getNumber() << " *= " << Three.getNumber() << endl;
		First *= Three;
		cout << "�������� 10 (*=) - " << First.getNumber() << endl;

		cout << '\n' << First.getNumber() << " /= " << Two.getNumber() << endl;
		First /= Two;
		cout << "�������� 11 (/=) - " << First.getNumber() << endl;


		cout << "�������� 12 < - " << endl;
		if (First < Three)
			cout << First.getNumber() << "<" << Three.getNumber() << endl;
		else
			cout << First.getNumber() << ">" << Three.getNumber() << endl;

		cout << "�������� 13 > - " << endl;
		if (First > Four)
			cout << First.getNumber() << ">" << Four.getNumber() << endl;
		else
			cout << First.getNumber() << "<" << Four.getNumber() << endl;

		cout << "�������� 14 <= - " << endl;
		if (First <= Two)
			cout << First.getNumber() << "<=" << Two.getNumber() << endl;
		else
			cout << First.getNumber() << ">" << Two.getNumber() << endl;

		cout << "�������� 15 >= - " << endl;
		if (First >= First)
			cout << First.getNumber() << ">=" << First.getNumber() << endl;
		else
			cout << First.getNumber() << "<" << First.getNumber() << endl;

		cout << "�������� 16 == - " << endl;
		if (First == Three)
			cout << First.getNumber() << "==" << Three.getNumber() << endl;
		else
			cout << First.getNumber() << "!=" << Three.getNumber() << endl;

		cout << "�������� 17 != - " << endl;
		if (First != First)
			cout << First.getNumber() << "!=" << First.getNumber() << endl;
		else
			cout << First.getNumber() << "==" << First.getNumber() << endl;
		int p = 0;
		cout << "������� ������� ��� ���������� ����� " << Two.getNumber() << " � �������: " << endl;
		cin >> p;

		cout << '\n' << Two.getNumber() << " ^ " << p << endl;
		First = Two ^ p;
		cout << "�������� 18 (^) - " << First.getNumber() << endl;
	}
	catch (int error)
	{
		if (error == 1)
			cout << "������ �� 0 ������!!!" << endl;
	}
	return 0;
}