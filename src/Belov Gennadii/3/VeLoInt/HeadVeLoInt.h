﻿#ifndef _VeLoInt_H_
#define _VeLoInt_H_
#include <string>
#include <iostream>
#include <cmath>
#include <process.h>

using namespace std;

class VeLoInt {
private:
	string number;
	char sign = '+';
	
public:
	// конструкторы
	VeLoInt() : number("0") {}
	VeLoInt(string str) {
		int x = 0;
		if (str.front() == '-') {
			sign = '-';
			str.erase(str.begin());
		}
		for (auto el : str) {
			if (!isdigit(el)) {
				cerr << "initialization error" << endl;
				exit(1);
			}
				
			if (!x && el != '0')              // 000
				x = 1;
		}
		number = x ? str : "0";
		if (number != "0")
			number.erase(0, number.find_first_not_of('0'));
	}
	VeLoInt(long long str) : number(to_string(str)) {
		if (number.front() == '-') {
			sign = '-';
			number.erase(number.begin());
		}
	}

	// потоки
	friend ostream& operator << (ostream& s, const VeLoInt& n)
	{
		if (n.sign == '+')
			s << n.number;
		else s << n.sign << n.number;
		return s;
	}
	friend istream& operator >> (istream& s, VeLoInt& n)
	{
		string str;
		s >> str;
		n = str;
		return s;
	}
	// сравнение и логика
	bool operator > (const VeLoInt& n) const;
	bool operator >= (const VeLoInt& n) const;
	bool operator < (const VeLoInt& n) const;
	bool operator <= (const VeLoInt& n) const;
	bool operator == (const VeLoInt& n) const;
	bool operator != (const VeLoInt& n) const;
	bool operator < (const int& n) const {
		VeLoInt temp = n;
		return *this < temp;
	}
	//bool operator ! () const;
	//bool operator && (const VeLoInt& n) const;
	//bool operator || (const VeLoInt& n) const;
	// заменил приведением типа
	operator bool() const {
		return number != "0" ? 1 : 0;     
	}


	VeLoInt operator -() const {
		VeLoInt temp = *this;
		temp.sign = sign != '-' ? '-' : '+';
		return temp;
	}

	// арифметика
	VeLoInt operator + (const VeLoInt& n) const {
		string x = sign == '-' ? "-" : "";
		return sign == n.sign ? x + sum(n.number) : diff(n.number);
	}
	VeLoInt operator - (const VeLoInt& n) const {
		string x = sign == '-' ? "-" : "";
		return sign != n.sign ? x + sum(n.number) : diff(n.number);
	}
	VeLoInt operator * (int n) const;
	VeLoInt operator * (const VeLoInt& n) const;
	VeLoInt operator / (const VeLoInt& n) const;
	VeLoInt operator % (const VeLoInt& n) const;

	VeLoInt operator ^ (int degree);              // степень
	VeLoInt operator ^ (const VeLoInt& degree);  // 8)


	VeLoInt operator += (const VeLoInt& n) {
		return *this = *this + n;
	}
	VeLoInt operator -= (const VeLoInt& n) {
		return *this = *this - n;
	}
	VeLoInt operator *= (const VeLoInt& n) {
		return *this = *this * n;
	}
	VeLoInt operator /= (const VeLoInt& n) {
		return *this = *this / n;
	}
	VeLoInt operator %= (VeLoInt& n) {
		return *this = *this % n;
	}
	VeLoInt operator ^= (int degree) {
		return *this = *this ^ degree;
	}


	VeLoInt operator++()
	{
		return *this += 1;
	}
	VeLoInt operator++(int)    // пост.
	{
		return *this += 1;
	}

	VeLoInt operator--()
	{
		return *this -= 1;
	}
	VeLoInt operator--(int)    // пост.
	{
		return *this -= 1;
	}

	// доп. методы
	string div(string number2, int y) const;
	string diff(string number2) const;
	string sum(string number2) const;
};

static VeLoInt operator "" _vli(const char* s) {      // литерал для VeLOInt
	return s;
}

#endif // !_VeLoInt_H_
