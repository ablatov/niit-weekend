﻿#include "HeadVeLoInt.h"

int main()
{
	// инициализация
	VeLoInt a;
	VeLoInt b = 751789875;
	VeLoInt c = "156574353454315143444844511515615151565743534543151434448445115156151515657435345431514344484451151561515";
	VeLoInt d = 4556568135155454654654345461189898797546151_vli;
	VeLoInt e = "00000000000000000000000000000000000000000000000000000000000000000008";
	VeLoInt f = c;
	// -
	VeLoInt g = -007;
	VeLoInt h = "-156574353454315143444844511515615151565743534543151434448445115156151515657435345431514344484451151561515";
	VeLoInt i = -4556568135155454654654345461189898797546151_vli;
	VeLoInt j = -c;

	VeLoInt s;  // для ввода
	VeLoInt z;
	z = 0;
	a = -e;

	// поток
	cout << "s = ";
	cin >> s;
	cout << "a = " << a << '\n' << "c = " << c << '\n' << "i = " << i << '\n' << endl;

	// + -
	cout << "\n\n\t + -" << endl;
	g = c + d;
	cout << g << '\n' << d + c << endl;
	g = c - d;
	cout << g << '\n' << d - c << endl;
	g = -c + d;
	cout << g << '\n' << d + -c << endl;
	g = -c - d;
	cout << g << '\n' << d - -c << endl;

	g = h + i;
	cout << g << '\n' << i + h << endl;
	g = c - i;
	cout << g << '\n' << i - c << endl;
	g = -h + d;
	cout << g << '\n' << d + -h << endl;
	g = -h - i;
	cout << g << '\n' << i - -h << endl;

	// * / %
	cout << "\n\n\t * / %" << endl;
	g = c * d;
	cout << g << '\n' << d * c << endl;
	g = c / d;
	cout << g << '\n' << d / c << endl;
	g = c % d;
	cout << g << '\n' << d % c << endl;

	g = h * i;
	cout << g << '\n' << i * h << endl;
	g = c / i;
	cout << g << '\n' << i / c << endl;
	g = h % d;
	cout << g << '\n' << d % h << endl;

	// ^
	cout << "\n\n\t ^" << endl;
	g = c ^ 3;
	cout << g << endl;
	g = c ^ -3;
	cout << g << endl;
	//g = c ^ d;                       // работает
	//cout << g << endl;
	
	// += -= *= /= % = ^=
	cout << "\n\n\t += -= *= /= % = ^=" << endl;
	g = 0_vli;
	g += c;
	cout << g << endl;
	g -= c;
	cout << g << endl;
	g = c;
	g /= c;
	cout << g << endl;
	g *= c;
	cout << g << endl;
	g %= d;
	cout << g << endl;
	g ^= 0;
	cout << g << endl;

	// < > <= >= != ==
	cout << "\n\n\t < > >= <= != ==" << endl;
	g = c < d;
	cout << g << endl;
	bool x;
	x = c > d;
	cout << x << endl;
	x = c >= d;
	cout << x << endl;
	x = c <= c;
	cout << x << endl;
	x = c != c;
	cout << x << endl;
	x = c += c;
	cout << x << endl;

	// ! && || 
	cout << "\n\n\t ! && ||" << endl;
	g = (!g && i) || (!e || b);
	cout << g << endl;

	if (g) {
		g += a * j / (c + h);   // проверка / 0
	}
	
	return 0;
}