﻿
#ifndef _AUTOMATA_H_
#define _AUTOMATA_H_
#include <string>
#include <clocale>

using namespace std;

enum STATES
{
	OFF, WAIT, ACCEPT, CHECK, COOK
};
class Automata
{
private:
	STATES state;               // текущее состояние автомата
	int cash;                   // для хранения текущей суммы
	int drink;                  // выбранный напиток
	int drinks [5];                // кол-во проданных напитков (по категориям)
	static const string menu[5];        // название напитков
	static const int prices[5];         // цена напитков
	static const string states[5];      // строковое определение состояния автомата
	static const string passWord;       // пароль для включения и выключения автомата
	
public:
	Automata(): state(OFF) { off(); }  // конструктор
	void on(); // включение автомата;
	void off(); // выключение автомата;
	void coin(int money); // занесение денег на счёт пользователем;
	void printMenu(); // отображение меню с напитками и ценами для пользователя;
	void printState(); // отображение текущего состояния для пользователя;
	void choice(); // выбор напитка пользователем;
	void check(); // проверка наличия необходимой суммы;
	void cancel(); // отмена сеанса обслуживания пользователем;
	void cook(); // имитация процесса приготовления напитка;
	void finish(); // завершение обслуживания пользователя.
	void pass();  // проверка пароля для выключения
	void printCook(int);     // арт
};


#endif // !_AUTOMATA_H_