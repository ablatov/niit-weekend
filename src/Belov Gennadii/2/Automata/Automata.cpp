﻿
#include "Automata.h"
#include <iostream>
#include <cstdlib>
#include <Windows.h>
//#include <algorithm>
#include <fstream>


// неизменяемые данные
const string Automata::menu[5] = { "1* Чай", "2* Кофе", "3* Глинтвейн", "4* Латте", "5* Угадай" };
const int Automata::prices[5] = { 15 , 20, 50, 35, 150 };
const string Automata::states[5] = { "OFF", "WAIT", "ACCEPT", "CHECK", "COOK" };
const string Automata::passWord = "gfhjkm";

void Automata::off() {
	drink = 0;
	cash = 0;
	//fstream stat("stat.dat", ios_base::in | ios_base::out | ios::binary);  // ? no work
	if (state == OFF) {
		ifstream stat("stat.dat", ios::binary);
		if (!stat)
			//fill_n(drinks, 5, 0);                    // иницилизация кол-ва наптков 0
		for (int & el : drinks)                
		el = 0;
		//memset(drinks, 0, sizeof(drinks));   // ?
		else {
			stat.read((char*)drinks, sizeof(drinks));        // загрузка статистики приготовленных напитков из файла
			stat.close();
		}
	}
	else {
		// при выключении после работы записываются статистические данные
		ofstream stat("stat.dat", ios::binary);       
		ofstream stat1("statistics.txt");
		int sum = 0;
		for (int i = 0; i < 5; i++) {
			int sumi = drinks[i] * prices[i];
			sum += sumi;
			stat1 << menu[i] << ' ' << drinks[i] << " шт. доход: " << sumi << " р." << endl;
		}
		stat1 << "Общий доход: " << sum << " р." << endl;
		stat.write((char*)drinks, sizeof(drinks));
		stat.close();
		stat1.close();
	}



	//*this ^= *this;)

	// пароль техника для включения
	string pass;
	while (1) {
		system("cls");
		cout << "\tАвтомат выключен.\n Для возобновления работы введите пароль: ";
		cin >> pass;
		if (pass == passWord)
			on();
		else {
			cout << "неа:)";
			cin.clear();
			cin.ignore(100000, '\n');
			Sleep(1000);
		}
	}

}

void Automata::on() {
	state = WAIT;
	printMenu();
	printState();  
	coin(0);     // автомат готов принимать деньги
}



void Automata::coin(int money) {
	
	if (money) {
		state = ACCEPT;
		cash += money;
		printState();
	}
	else {
		// первоначальное состояние
		cout << "Внесите деньги: " << endl;
		cout << "Для окончания операции нажмите Q" << endl;  // любую клавишу (иммитация)
	}
	
	// while (!drink && cash - prices[drink] >= 0)  // для кнопок
	while (1) {
		cin >> money;
		if (cin.fail()) {
			pass();         // праверка: был ли введен пароль для выключения автомата
			choice();
		}	
		else coin(money);    // ещё монетка)
	}


		
}

void Automata::choice() {
	state = CHECK;
	printState();
	if (!drink) {
		cout << "Выберете напиток: ";
		int itemMenu;

		while (1) {
			cin >> itemMenu;
			switch (itemMenu) {
			case 1:	case 2:	case 3: case 4: case 5:
				drink = itemMenu;
				check();               // только пять напитков
			default:
				pass();				            // всё остальное - ошибка ввода или пароль
				cout << "Ошибка ввода\n";				
			}
		}
	}
	check();   // при добавлении суммы
		
}
void Automata::check() {
	
	printState();
	if (cash - prices[drink - 1] >= 0)
		cook();
	else {
		cout << "Внесённых денег недостаточно" << endl;
		cancel();             // выбор при несоответсвии суммы и напитика
	}
}

void Automata::cook() {
	state = COOK;
	printState();
	cout << "Идёт приготовление напитка" << endl;
	for (int color = 1; color < 16; color++)       // иммитация процесса
		printCook(color);
	finish();    // сдача и статистика

}

void Automata::finish() {
	cout << "Ваш напиток готов" << endl;
	cout << "Ваша сдача: " << cash - prices[drink - 1] << " р." << endl;
	Sleep(7000);
	++drinks[drink - 1];
	drink = 0;
	cash = 0;
	on();   // в состояние ожидания нового клиента
}

void Automata::cancel() {
	state = WAIT;
	printState();
	cout << "Выберите дальнейшие действия:" << endl;
	cout << "1 - Добавить денег\n" << "2 - Выбрать другой напиток\n" << "3 - Вернуть деньги\n";
	
	int itemMenu;
	while (1) {
		cin >> itemMenu;
		switch (itemMenu) {
		case 1:
			coin(0);    // сохранены cash и выбранный напиток
			break;
		case 2:
			drink = 0;
			choice();     // при той же внесённой сумме
			break;
		case 3:
			cout << "Ваша сдача: " << cash << " р." << endl;
			cash = 0;
			drink = 0;
			Sleep(7000);
			on();        // вернули деньги, ожидаем нового клиента
			break;
		default:
			pass();                     // иначе ошибка ввода или пароль
			cout << "Ошибка ввода\n";
		}
	}

}



void Automata::printMenu() {
	int i = 0;
	system("cls");
	cout << "\tМеню\n";
	for (auto m : menu)
		cout << m << ' ' << prices[i++] << "р.\n";
	cout << endl;
}

void Automata::printState() {
	COORD bufferSize = { 50, 1 };
	SMALL_RECT writeArea = { 35,0,95,1 };
	COORD charPosition = { 0,0 };
	CHAR_INFO consBuffer[50] = {};
	int i = 0;
	string str = " / State: " + states[state] + " / Cash: " + to_string(cash) + "p." +
		" / Drink: " + to_string(drink); 
	for (auto el : str) {
		consBuffer[i].Char.AsciiChar = el;
		consBuffer[i++].Attributes = 7;
	}
	WriteConsoleOutput(GetStdHandle(STD_OUTPUT_HANDLE), consBuffer, bufferSize, charPosition, &writeArea);
	// русский не выводит?
}

void Automata::pass() {
	cin.clear();
	string pass;
	getline(cin, pass);       // то что в буфере осталось
	if (pass == passWord)
		off();
}

void Automata::printCook( int color) {
	COORD bufferSize = { 27, 17 };
	SMALL_RECT writeArea = { 40,3,69,20 };
	COORD charPosition = { 0,0 };
	CHAR_INFO consBuffer[27*17] = {};
	int i = 0;
	string str = "                 8         "
		"                88         "
		"                888        "
		"               888         "
		"              88   8       "
		"             8    88       "
		"                  8        "
		"                           "
		"     88888888888888888888  "
		"     88________________8888"
		"      88______________88  8"
		"       88____________88888 "
		"         888_______888     "
		"     888****88888888****888"
		"      8888***********8888  "
		"         8888888888888     ";
	for (auto el : str) {
		consBuffer[i].Char.AsciiChar = el;
		consBuffer[i++].Attributes = color;
	}
	WriteConsoleOutput(GetStdHandle(STD_OUTPUT_HANDLE), consBuffer, bufferSize, charPosition, &writeArea);
	Sleep(1000);
}
