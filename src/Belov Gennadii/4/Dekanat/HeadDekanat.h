﻿#ifndef _HeadDekanat_H_
#define _HeadDekanat_H_
#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include <memory>
#include <fstream>
#include <clocale>

using namespace std;
static int counter = 1;
class Dekanat;
class Group;
// Студент
class Student {
private:
	int ID;                                             // идентификационный номер
	string Fio;                                         // фамилия и инициалы
	Group *group;                                       // ссылка на группу
	Dekanat * dekanat;                                  // ссылка на деканат
	int Marks[5];                                       // массив оценок
	int Num;                                            // количество оценок
public:
	// конструкторы 
	// студент может быть не зачислен в группу
	Student() : ID(counter++), Fio(""), Num(0), group(nullptr), dekanat(nullptr) {}
	Student(string fio) : Student() { Fio = fio; }  // создание студента
	Student(string fio, Dekanat *d) : Student() { Fio = fio; dekanat = d; }
	

	void EnlistmentGroup(Group *groupa);        // зачисление в группу	
	void AddMark(int mark);       // добавление оценки
	int sumMarks() const;        // сумма всех оценок
	float MidMark() const;        // вычисление средней оценки
	// пр. методы
	// получение данных студента
	Group* getGroup() const { return group;	}
	int getID() const { return ID; }
	string getFio() const { return Fio; }	
};

// Группа
class Group {
private:
	string Title;                                   // название группы
	vector<Student*> Students;                     // массив из ссылок на студентов
	int Num;                                      // количество студентов в группе
	Student *HeadStudent;                        // ссылка на старосту
	Dekanat * dekanat;                          // ссылка на деканат
public:
	// конструкторы
	// группа может быть без студентов
	Group() : Title(""), Num(0), HeadStudent(nullptr), dekanat (nullptr){}   // создание группы
	Group(string name) :Group() { Title = name; };
	Group(string name, Dekanat *d) :Group() { Title = name; dekanat = d; };
	
	void AddStudent(Student *student);         // добавление студента в группу
	void exStudent(Student *student);          // исключение студента из группы	
	void ElectStudent()	{ HeadStudent = Students[rand() % Num]; }    // избрание старосты

	// поиск студента по ФИО или ИД
	Student* SearchStudent(int id) const;
	Student* SearchStudent(string fio) const;

	float MidMarkGroup() const;             // вычисление среднего балла в группе

	
	// пр. методы
	vector<Student*> getStudents() const { return Students; }
	string getTitle() const { return Title; }
	string getStud() const;
	bool prStudent(Student *student);	
};

// Деканат
class Dekanat {
private:
	vector<unique_ptr<Student>> Students;          // массив из ссылок на студентов
	vector<unique_ptr<Group>> Groups;              // массив из ссылок на группы	
public:
	// конструкторы
	Dekanat() {};
	Dekanat(vector<string> nameFile, int x);

	void AddRandMark();        // добавление случайных оценок студентам
	void AcademicPerformance() const;     // накопление статистики по успеваемости студентов и групп
	void StudentTransfer(Student *student, Group *newGroup);    // перевод студентов из группы в группу
	// проверка на присутствие студента и группы в деканате
	bool prStudent(Student *student);
	bool prGroup(Group *group);

	void ElectStudentGr();       // инициация выборов старост в группах
	void exStudentsAP();        // отчисление студентов за неуспеваемость
	void inConsole() const;      //вывод данных на консоль
	void inFile() const;          //сохранение обновленных данных в файлах
	void grouping();           // случайное распределение студентов

	Student * getStud(int i) const { return Students[i].get(); }
	Group * getGroup(int i) const { return Groups[i].get(); }
	
};
#endif // !_HeadDekanat_H_
