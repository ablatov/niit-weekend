﻿#ifndef _HeadStaff_H_
#define _HeadStaff_H_
#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <fstream>
#include <ctime>
#include <map>
#include <iomanip>
using namespace std;


class Employee {
protected:
	int ID;
	string Name;	
	int Payment;	
public:
	~Employee() {}
	virtual void setData(istream &str) {       // получение данных
		string strEmp;
		getline(str, strEmp, '/');
		ID = stoi(strEmp);
		getline(str, strEmp, '/');
		Name = strEmp;
	}
	virtual void payroll() = 0;  // начисление зарплат
	virtual void output() = 0;   // вывод на экран
};

// пример с рабочими классами WorkTime, Project, Heading
class WorkTime {
protected:
	int workTime;

public:	
	virtual ~WorkTime() {}
	void setWorkTime(int time) { workTime = time; }
	int payWork(int rate) { return workTime * rate; }
};

class Project {
protected:
	vector<string> nameProjects;
	map <string, int> Projects = { { "Bank", 5000 },{ "Game", 3000 },{ "Hospital", 7000 },
	             { "Library", 4000 },{ "School", 6000 } };	
public:	
	virtual ~Project() {}
	void setNameProject(string name) { nameProjects.push_back(name); }
	int payProject(float share) {
		int sum = 0;
		for (auto el : nameProjects) {
			sum += Projects.at(el);
		}
		return sum * share;
	}
};

class Heading {
protected:
	int qtPeople;
public:	
	virtual ~Heading() {}
	void setqtPeople(int qt) { qtPeople = qt; }
	int  payHeading(int rate) { return qtPeople * rate; }
};


// классы наследования

class Personal: public Employee, public WorkTime{  // абстрактный
protected:
	int rate;
public:	
	virtual ~Personal() {}
	virtual void payroll() {
		Payment = payWork(rate);
	}
	virtual void setData(istream &str) {
		Employee::setData(str);
		string strPers;
		getline(str, strPers);
		rate = stoi(strPers);
		setWorkTime(rand() % 170 + 50);
	}
};

class Cleaner : public Personal {
private:
	static constexpr int premium = 500;	
public:	
	virtual ~Cleaner() {}
	virtual void payroll() {
		Personal::payroll();
		if (workTime > 100)
			Payment += premium;
	}
	virtual void output() {		
		cout << setw(4) << ID << setw(30) << left << Name
			<< setw(16) << "Cleaner" << setw(11) << workTime
			<< setw(13) << "-" << setw(10) << Payment << endl;		
	}
};

class Driver : public Personal {
private:
	static constexpr int rateOvertime = 50;	
public:	
	virtual ~Driver() {}
	virtual void payroll() {

		if (workTime < 100)
			Personal::payroll();
		else {
			Payment = 100 * rate + (workTime - 100) * rateOvertime;
		}
	}
	virtual void output() {		
		cout << setw(4) << ID << setw(30) << left << Name
			<< setw(16) << "Driver" << setw(11) << workTime
			<< setw(13) << "-" << setw(10) << Payment << endl;		
	}
};

class Engineer : public Employee, public WorkTime, public Project {    // абстрактный
protected:
	int rateDay;
	float bonus;
public:	
	virtual ~Engineer() {}
	virtual void payroll() {
		Payment = payWork(rateDay/8) + payProject(bonus);
	}
	virtual void setData(istream &str) {
		Employee::setData(str);
		string strPers;
		getline(str, strPers, '/');
		rateDay = stoi(strPers);
		while (1) {
			getline(str, strPers, '/');
			if (strPers.find_first_not_of("0123456789"))
				break;
			setNameProject(strPers);
		}		
		bonus = stof(strPers);
		setWorkTime(rand() % 240 + 50);				
	}	
};

class Programmer : public Engineer {
private:
	static constexpr float premiumBonus = 0.1;
	
public:	
	virtual ~Programmer() {}
	virtual void setData(istream &str) {
		Engineer::setData(str);
		string stren; 
		getline(str, stren); 
	}
	virtual void payroll() {
		if (nameProjects.size() > 2) {
			bonus += premiumBonus;
		}
		Engineer::payroll();			
	}
	virtual void output() {
		
		cout << setw(4) << ID << setw(30) << left <<  Name
			<< setw(16) << "Programmer" << setw(11) << workTime
			<< setw(13) << nameProjects.size() << setw(10) << Payment << endl;		
	}
};

class Tester : public Engineer {
private:
	int rate;
	
public:	
	virtual ~Tester() {}
	virtual void setData(istream &str) {
		Engineer::setData(str);
		string stren;
		getline(str, stren);
	}
	virtual void payroll() {
		if (workTime > 200) {
			rate = rateDay / 8 + 30;
			Payment = payWork(rate / 8) + payProject(bonus);
		}
		else Engineer::payroll();
	}
	virtual void output() {
		cout << setw(4) << ID << setw(30) << left << Name
			<< setw(16) << "Tester" << setw(11) << workTime
			<< setw(13) << nameProjects.size() << setw(10) << Payment << endl;		
	}
};

class TeamLeader : public Programmer, public Heading {
private:
	int ratePe;
public:
	virtual ~TeamLeader() {}
	virtual void setData(istream &str) {
		Engineer::setData(str);
		string stren; 
		getline(str, stren);
		ratePe = stoi(stren);
		setqtPeople(rand() % 2 + 4);
	}
	virtual void payroll() {		
		Programmer::payroll();
		Payment += payHeading(ratePe);
	}
	virtual void output() {		
		cout << setw(4) << ID << setw(30) << left << Name
			<< setw(16) << "TeamLeader" << setw(11) << workTime
			<< setw(13) << nameProjects.size() << setw(10) << Payment << endl;
	}
};

class Manager : public Employee, public Project {   // абстрактный
protected:	
	float bonus;
public:
	virtual ~Manager() {}
	virtual void payroll() {
		Payment = payProject(bonus);
	}
	virtual void setData(istream &str) {
		Employee::setData(str);
		string strPers;		
		while (1) {
			getline(str, strPers, '/');
			if (strPers.find_first_not_of("0123456789"))
				break;
			setNameProject(strPers);
		}
		bonus = stof(strPers);				
	}
};

class ProjectManager : public Manager, public Heading {
private:
	static constexpr float premiumBonus = 0.3;
protected:
	int ratePe;
public:
	virtual ~ProjectManager() {}
	virtual void setData(istream &str) {
		Manager::setData(str);
		string stren;
		getline(str, stren);
		ratePe = stoi(stren);
		setqtPeople(rand() % 5 + 5);
	}
	virtual void payroll() {
		if (nameProjects.size() > 2) {
			bonus += premiumBonus;
		}
		Manager::payroll();
		Payment += payHeading(ratePe);
	}
	virtual void output() {		
		cout << setw(4) << ID << setw(30) << left << Name
			<< setw(16) << "ProjectManager" << setw(11) << "-"
			<< setw(13) << nameProjects.size() << setw(10) << Payment << endl;		
	}
};

class SeniorManager : public ProjectManager {
private:
	static constexpr float premiumBonus = 0.5;

public:
	virtual ~SeniorManager() {}
	virtual void setData(istream &str) {
		ProjectManager::setData(str);		
		setqtPeople(rand() % 10 + 7);
	}
	virtual void payroll() {
		if (nameProjects.size() < 2) {
			bonus -= premiumBonus;
		}
		else bonus += premiumBonus;
		Manager::payroll();
		Payment += payHeading(ratePe);
	}
	virtual void output() {		
		cout << setw(4) << ID << setw(30) << left << Name
			<< setw(16) << "SeniorManager" << setw(11) << "-"
			<< setw(13) << nameProjects.size() << setw(10) << Payment << endl;		
	}
};

class DepStaff {                   // обёртка
private:
	//vector<unique_ptr<Employee>> Employees;   // происходила утечка памяти
	vector<shared_ptr<Employee>> Employees;	    // с этим указателем всё хорошо
public:
	DepStaff() {}
	~DepStaff() {
		while (Employees.size())
		{
			Employees.erase(Employees.begin());   // вроде всё должно удаляться
		}
		cout << "y";
		/*auto end = Employees.end();
		for (auto i = Employees.begin(); i != end; ++i) {
			(*i) = nullptr;
		}*/
	}
	DepStaff(string nameFile) {
		ifstream fEmployees(nameFile);
		if (fEmployees.fail())
			cerr << "file error" << endl;
		string z;
		getline(fEmployees, z);
		while (!fEmployees.eof()) {
			string pers;
			getline(fEmployees, pers, '/');
			if (pers == "Cleaner")
				Employees.push_back(make_shared<Cleaner>());
			else if (pers == "Driver")
				Employees.push_back(make_shared<Driver>());
			else if (pers == "Programmer")
				Employees.push_back(make_shared<Programmer>());
			else if (pers == "Tester")
				Employees.push_back(make_shared<Tester>());
			else if (pers == "TeamLeader")
				Employees.push_back(make_shared<TeamLeader>());
			else if (pers == "ProjectManager")
				Employees.push_back(make_shared<ProjectManager>());
			else if (pers == "SeniorManager")
				Employees.push_back(make_shared<SeniorManager>());
			Employees.back()->setData(fEmployees);
		}
		fEmployees.close();

	}

	void depPayroll() {                                    // начисление зарплат
		auto end = Employees.end();
		for (auto i = Employees.begin(); i != end; ++i) {
			(*i)->payroll();
		}
	}
	void depDisplay() {                              // вывод на экран
		cout << "\t\t Информация о сотрудниках" << endl;
		cout << "ID# " << setw(29) << left << " Сотрудник "
			<< setw(15) << "Должность" << setw(6) << "Время"
			<< setw(17) << "Кол-во проектов"	<< setw(2) << "Зарплата" << endl;
		auto end = Employees.end();
		for (auto i = Employees.begin(); i != end; ++i) {
			(*i)->output();
		}
	}
};
#endif // !_HeadStaff_H_