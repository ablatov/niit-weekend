﻿#include "Circle.h"

void Earth_Rope() {
	const  double radEarth = 6378100.;          //  радиус Земли 6378.1 км 
	double pieceRope;
	
	Circle Earth, Rope;
	Earth.setRadius(radEarth);
	Rope = Earth;
	cout << "Укажите значение, на которое будет увеличена длина верёвки (в метрах): ";
	checkInput(pieceRope);
	
	//cout.setf(ios::internal);
	
	Rope.setFerence(Rope.getFerence() + pieceRope);
	cout << "Зазор между поверхностью планеты и верёвкой будет равен: "
		<< fixed << setprecision(3)
		<< Rope.getRadius() - Earth.getRadius() << " м" << endl;
}