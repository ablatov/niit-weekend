﻿#include "Circle.h"
#define _USE_MATH_DEFINES
//#include <cmath>
#include <math.h>

//const double Circle::PI = 3.14159265;

void Circle::setRadius(double rad) {
	Radius = rad;
	Ference = 2 * M_PI * Radius;
	Area = M_PI * Radius * Radius;
}
void Circle::setFerence(double ference) {
	Ference = ference;
	Radius = Ference / (2 * M_PI);
	Area = M_PI * Radius * Radius;
}
void Circle::setArea(double area) {
	Area = area;
	Radius = sqrt(area / M_PI);
	Ference = 2 * M_PI * Radius;
}

void checkInput(double& value) {
	while (1) {
		cin >> value;
		if (cin.fail())
		{
			cout << "Ошибка ввода\n"
				<< "Попробуйте ещё раз: ";
			cin.clear();
			cin.ignore(100000, '\n');
			//cin.sync();
			continue;
		}
		break;
	}
}
