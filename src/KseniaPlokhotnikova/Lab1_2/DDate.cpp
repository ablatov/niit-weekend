#include "DDate.h"
#include <time.h>
#include <cstdio>
#include <algorithm>
DDate::DDate()
{
	time_t rawtime = time(NULL);
	tm *localTm = localtime(&rawtime);
};
DDate::DDate(int day, int month, int year)
{
	
	day = _date.tm_mon;
	month = _date.tm_mday;
	year = _date.tm_year;
};

void DDate::printToday()
{
	time_t rawtime = time(NULL);
	tm *localTm = localtime(&rawtime);

	if (!localTm)
		return;

	printf("Local time: %i.%i.%i_%i:%i:%i\n",
		localTm->tm_mday,
		localTm->tm_mon + 1,
		localTm->tm_year + 1900,
		localTm->tm_hour,
		localTm->tm_min,
		localTm->tm_sec);
	
		
}
void DDate::printTomorrow()
{
	time_t rawtime = time(NULL);
	tm *localTm = localtime(&rawtime);

	if (!localTm)
		return;
	localTm->tm_mday += 1;
	time_t next = mktime(localTm);
	tm *local = localtime(&next);
	printf("Local time: %i.%i.%i_%i:%i:%i\n",
		local->tm_mday,
		local->tm_mon + 1,
		local->tm_year + 1900,
		local->tm_hour,
		local->tm_min,
		local->tm_sec,
		local->tm_mday
		
	);
	
};
void DDate::printYesterday()
{
	time_t rawtime = time(NULL);
	tm *localTm = localtime(&rawtime);

	if (!localTm)
		return;
	localTm->tm_mday -= 1;
	time_t next = mktime(localTm);
	tm *local = localtime(&next);
	printf("Local time: %i.%i.%i_%i:%i:%i\n",
		local->tm_mday,
		local->tm_mon + 1,
		local->tm_year + 1900,
		local->tm_hour,
		local->tm_min,
		local->tm_sec);
	
}
void DDate::printPast(int numberOfDays)
{
	if (numberOfDays < 0)
		return;
	time_t rawtime = time(NULL);
	tm *localTm = localtime(&rawtime);

	if (!localTm)
		return;
	localTm->tm_mday -= numberOfDays;
	time_t next = mktime(localTm);
	tm *local = localtime(&next);
	printf("Local time: %i.%i.%i_%i:%i:%i\n",
		local->tm_mday,
		local->tm_mon + 1,
		local->tm_year + 1900,
		local->tm_hour,
		local->tm_min,
		local->tm_sec);
	
}
void DDate::printFuture(int numberOfDays)
{
	if (numberOfDays < 0)
		return;
	time_t rawtime = time(NULL);
	tm *localTm = localtime(&rawtime);

	if (!localTm)
		return;
	localTm->tm_mday += numberOfDays;
	time_t next = mktime(localTm);
	tm *local = localtime(&next);
	printf("Local time: %i.%i.%i_%i:%i:%i\n",
		local->tm_mday,
		local->tm_mon + 1,
		local->tm_year + 1900,
		local->tm_hour, 
		local->tm_min,
		local->tm_sec);
	
}

time_t DDate::calculateDiff(tm& first_date, tm& second_date)
{
	return (time_t) std::abs(
		mktime(&first_date) - mktime(&second_date)) / SECONDS_IN_DAYS;
}
void DDate::printCurrentWeekDay()
{
	time_t rawtime = time(NULL);
	tm *localTm = localtime(&rawtime);

	if (!localTm)
		return;
	switch (localTm->tm_wday)
	{
	case 0: printf(" sunday"); break;
	case 1:printf("monday "); break;
	case 2: printf(" tuesday");     break;
	case 3:printf(" wednesday");       break;
	case 4:printf("thursday");     break;
	case 5:printf(" friday");     break;
	case 6: printf("saturday");     break;
	}
};
