#pragma once
#include <time.h>

#define SECONDS_IN_DAYS (86400)

class DDate
{
private:
	struct tm _date;
public:
	DDate();
	DDate(int day, int month, int year);
	static void printToday();
	static void printTomorrow();
	static void printYesterday();
	static void printPast(int numberOfDays);
	static void printFuture(int numberOfDays);
	static time_t calculateDiff(tm & first_date, tm & second_date);
	static void DDate::printCurrentWeekDay();
};