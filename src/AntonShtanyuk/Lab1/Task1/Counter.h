#ifndef _COUNTER_H_
#define _COUNTER_H_

class Counter
{
private:
    int value;
public:
    Counter():value(0) {}
    void inc();
    void dec();
    void reset();
    int get() const;
};

#endif