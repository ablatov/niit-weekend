#include<string>
#include<sstream>
#include<iostream>
#include<list>
#include<iterator>
#include<fstream>
#include<ctime>
using namespace std;

const int quantity_st_gr = 10;//������������ ����� ��������� � ������
const string excl = "Excl";//����������� �������� ������ �����������
const float border = 3;//����� ������������ ������������ ���������
const int quantity_students = 21;//���������� ��������� ��� ����������� �������� �� �����
const int quantity_group = 3;//���������� ����� ��� ����������� �������� �� �����

class Group;
class Student
{
private:
	int id;
	string fio;
	string marks;
	int num;
	Group*gr;
public:
	Student() {};
	Student(string st)
	{
		int count = 0;
		stringstream is;
		is << st;
		stringstream id_;
		while (is)
		{
			string s;
			getline(is, s, ';');
			if (count == 0)
			{
				id_ << s;
				id_ >> this->id;
				id_.str("");
				id_.clear();
			}
			else if (count == 1){this->fio = s;}
			else if(count==2)
			{

				this->marks = s;
			}
			count++;
		}
		this->num =marks.length();
		this->gr = NULL;
	}
	int get_id()const{return this->id;}
	string get_fio()const{return this->fio;}
	string get_marks()const { return this->marks; }
	Group&get_gr()const { return (*this->gr);}
	void add_mark(int m)
	{
		string s = to_string(m);
		marks += s;
		num++;
	}
	float calc_marks()
	{
		float sum = 0;
		float num;
		for (int i = 0; i < marks.size(); i++)
		{
			num = marks.at(i)-48;
			sum += num;
			num = 0;
		}
		return sum/this->num;
	}
	void add_group(Group*gr){this->gr = gr;}
	void delete_group(){this->gr = NULL;}
	void show()
	{
		cout << id << "."<<fio<<endl;
		cout << "marks: ";
		cout << marks << endl;
	}
};
class Group
{
private:
	string title;
	list<Student*>arr;
	Student*head;
public:
	Group() {};
	Group(string buff)
	{
		this->title = buff;
		head = nullptr;
	}
	Group(const Group& gr)
	{
		this->title = gr.title;
		this->arr = gr.arr;
		this->head = gr.head;
	}
	string get_title()const { return this->title; }
	list<Student*>get_arr()const { return arr; }
	Student*get_head()const { return head; }

	void add_student(list<Student>&mass_st)
	{
		for (list<Student>::iterator it = mass_st.begin(); it != mass_st.end(); ++it)
		{
			if (!&(*it).get_gr())
			{
				arr.push_back(&(*it));
				(*it).add_group(this);
			}
		}
		elections();
	}
	void add_student(Student*st)
	{
		if (arr.size()< quantity_st_gr)
		{
			arr.push_back(st);
			(*st).add_group(this);
		}
		elections();
	}
	Student*search_stud(string fio)
	{
		for (list<Student*>::iterator it = arr.begin(); it != arr.end(); ++it)
		{
			if ((*(*it)).get_fio() == fio) 
			{ 
				return(*it);
			}
		}
	}
	bool search_stud(string fio,bool b)//��������� ��������
	{
		for (list<Student*>::iterator it = arr.begin(); it != arr.end(); ++it)
		{
			if ((*(*it)).get_fio() == fio)
				return true;
		}
		return false;
	}
	float calc_marks_gr()
	{
		float sum = 0;
		for (list<Student*>::iterator it = arr.begin(); it != arr.end(); ++it)
		{
			sum += (*(*it)).calc_marks();
		}
		return sum/arr.size();
	}
	void exclusion(Student*st)
	{
		for (list<Student*>::iterator it = arr.begin(); it != arr.end();++it)
		{
			if (*it == st)
			{
				(*(*it)).delete_group();
				arr.erase(it);
			}
		}
	}
	void elections()//������ �������� �� ������� �������
	{
		list<Student*>::iterator it = arr.begin();
		int temp_mark = (*(*it)).calc_marks();
		Student*temp_st = nullptr;
		for (it; it != arr.end(); ++it)
		{
			if ((*(*it)).calc_marks() > temp_mark)
			{
				temp_mark = (*(*it)).calc_marks();
				temp_st = (*it);
			}
		}
		this->head = temp_st;
	}
};
class Decanat
{
private:
	list<Student>mass_st;
    list<Group>mass_gr;
    static Decanat *p;
public:
	Decanat()
	{
	}
	static Decanat*create()
	{
		if(!p)
		p = new Decanat();
		return p;
	}
	void load_stud()
	{
		ifstream file_stud("stud.csv", ios_base::in);
		string buff_st[quantity_students];
		if (!file_stud.is_open())
		{
			cout << "File error" << endl;
			system("pause");
		}
		else
		{
			for (int i = 0; i < quantity_students; i++)
			{
				getline(file_stud, buff_st[i]);
				Student st(buff_st[i]);
				mass_st.push_back(st);
			}
		}
	}
	void load_gr()
	{
		ifstream file_gr("groups.csv", ios_base::in);
		string buff_gr[quantity_group];
		if (!file_gr.is_open())
		{
			cout << "File error" << endl;
			system("pause");
		}
		else
		{
			for (int i = 0; i <quantity_group; i++)
			{
				getline(file_gr, buff_gr[i]);
				Group gr(buff_gr[i]);
				mass_gr.push_back(gr);
			}
		}
	}
	list<Student>&get_students()
	{
		return mass_st;
	}
	void set_stud(Student&st)
	{
		mass_st.push_back(st);
	}
	void set_gr(Group&gr)
	{
		int count = 0;
		for (list<Group>::iterator it = mass_gr.begin(); it != mass_gr.end(); ++it)
		{
			if ((*it).get_title()== gr.get_title())
			{
				count++;
			}
		}
		if (count == 0)
		{
			mass_gr.push_back(gr);
		}
	}
	Group*get_gr(string title)
	{
		for (list<Group>::iterator it = mass_gr.begin(); it != mass_gr.end(); it++)
		{
			if ((*it).get_title() == title)return&(*it);
		}
	}
	void elections()
	{
		for (list<Group>::iterator it = mass_gr.begin(); it != mass_gr.end(); ++it) { (*it).elections(); }//��������� ������ ��������
	}
	void transfer(string fio_st,string title_gr)//������� �������� �� ������ � ������
	{
		Student*temp_st=nullptr;
		Group*temp_gr=nullptr,*temp_gr_tr=nullptr;
		for (list<Group>::iterator gr = mass_gr.begin(); gr != mass_gr.end(); ++gr)
		{
			if ((*gr).search_stud(fio_st, true))
			{
				temp_st = (*gr).search_stud(fio_st);//����� �������� �������� ���������
				temp_gr = &*gr;//����� ������ ������ ���������
				if ((*temp_gr).get_head()== temp_st)//���� �� ��������� ��������
				{
					(*gr).elections();//����� ������ ��������
				}
			}
			if ((*gr).get_title() == title_gr)
			{
				temp_gr_tr = &*gr; //����� ������ ���� ���������
			}
		}
		(*temp_gr).exclusion(temp_st);
		(*temp_gr_tr).add_student(temp_st);
	}
	void exclusion_stud()//���������� �� �� ������������
	{
		Group excl(excl);
		mass_gr.push_back(excl);

		list<Student>::iterator it = mass_st.begin();
		for (it; it !=mass_st.end(); ++it)
		{
			if ((*it).calc_marks() < border)
			{
				this->transfer((*it).get_fio(), "Excl");
			}
		}
	}
	void show_dec()
	{
		for (list<Group>::iterator it = mass_gr.begin(); it != mass_gr.end(); ++it)
		{
			string tit = (*it).get_title();
			cout << tit << endl;
			if (tit != excl) { cout << "Head_gr: " << (*(*it).get_head()).get_fio() << endl; }
			for (list<Student>::iterator it2 = mass_st.begin(); it2 != mass_st.end(); ++it2)
			{
				if (((*it2).get_gr()).get_title() == tit)
				{
					(*it2).show();
				}
			}
			cout << endl;
		}
	}
	void add_marks()
	{
		int mark = 0; 
		for (list<Student>::iterator it = mass_st.begin(); it != mass_st.end(); ++it)
		{
			if ((*it).get_gr().get_title() != "Excl")
			{
				mark = 2 + rand() % 4;
				(*it).add_mark(mark);
			}
		}
	}
	string stud_to_str(Student&st)
	{
		stringstream is;
		string s;
		is << st.get_id() << ";";
		is << st.get_fio() << ";";
		is >> s;
		s += st.get_marks();
		return s;
	}
	string gr_to_str(Group&gr)
	{
		stringstream is;
		string s;
		is << gr.get_title();
		is >> s;
		return s;
	}
	void save_file()
	{
		ofstream out_gr("groups.csv", ios_base::out, ios_base::trunc);
		ofstream out_stud("stud.csv", ios_base::out, ios_base::trunc);
		if (!out_gr.is_open() && !out_gr.is_open())
		{
			cout << "File Error w" << endl;
			system("pause");
		}
		else
		{
			for (list<Student>::iterator it_st=mass_st.begin();it_st!=mass_st.end();++it_st)
			{
				out_stud << stud_to_str((*it_st)) << "\n";
			}
			for (list<Group>::iterator it_gr = mass_gr.begin(); it_gr != mass_gr.end(); ++it_gr)
			{
				out_gr << gr_to_str((*it_gr)) << "\n";
			}
		}
	}
};
