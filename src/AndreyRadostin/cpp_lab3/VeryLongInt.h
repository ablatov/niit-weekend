#ifndef _VERYLONGINT_H_
#define _VERYLONGINT_H_
#include<iostream>

using namespace std;

class VeryLongInt
{
private:
	int size;
	int *arr;

public:

	VeryLongInt(char *str) {
		size =strlen(str);
		arr = new int[size];
		for (int i = 0; i < size; i++)
			*(arr+i) = *(str + size - i - 1)-'0';
		}
	VeryLongInt(int Size, int k) {
		size = Size;
		arr = new int[size];
		for (int i = 0; i < size; i++)
			arr[i] = k;
	}
	~VeryLongInt() {};
	VeryLongInt& operator= (const VeryLongInt & n1);
	void operator+= (const VeryLongInt & n1);
	void operator-= (const VeryLongInt & n1);
	void printNum() const;
	int getSize() const;
	
	int getNum(int i) const;
	
	void setNum(int i, int x);
	
};
VeryLongInt operator+ (const VeryLongInt & n1, const VeryLongInt & n2);
VeryLongInt operator- (const VeryLongInt & n1, const VeryLongInt & n2);
VeryLongInt operator* (const VeryLongInt & n1, const VeryLongInt & n2);
bool operator< (const VeryLongInt & n1, const VeryLongInt & n2);
bool operator> (const VeryLongInt & n1, const VeryLongInt & n2);
bool operator== (const VeryLongInt & n1, const VeryLongInt & n2);
bool operator!= (const VeryLongInt & n1, const VeryLongInt & n2);
bool operator>= (const VeryLongInt & n1, const VeryLongInt & n2);
bool operator<= (const VeryLongInt & n1, const VeryLongInt & n2);

#endif