#include "Circle.h"
#include<math.h>
#include<iostream>

using namespace std;

const double PI = 4.*atan(1.);

void Circle::setRadius(double radius)
{
	Radius = radius;
	Ference = 2 * PI*radius;
	Area = PI*pow(radius,2.0);
}

void Circle::setFerence(double ference)
{
	Ference = ference;
	Radius = ference / (2 * PI);
	Area = PI*Radius*Radius;
}

void Circle::setArea(double area)
{
	Area = area;
	Radius = sqrt(area / PI);
	Ference = 2 * PI*Radius;
}

double Circle::get(int k) const
{
	switch (k)
	{

	case 1:
	{
			  return Radius;
			  break;
	}
	case 2:
	{
			  return Ference;
			  break;
	}
	case 3:
	{
			  return Area;
			  break;
	}
	}
}

void Circle::reset()
{
	Radius = 0;
	Ference = 0;
	Area = 0;
}

	