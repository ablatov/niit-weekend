#ifndef _CIRCLE_H_
#define _CIRCLE_H_

class Circle
{
private:
	double Radius;
	double Ference;
	double Area;
public:
	void setRadius(double radius);
	void setFerence(double fererence);
	void setArea( double area);
	void reset();
	double get(int k) const;
	void printMessage();
	
};

#endif