#include "dekanat.h"
#include<iostream>
#include<fstream>
#include<list>
#include<vector>
#include<string>
#include<ctime>
#include<algorithm>
#include<iterator>

using namespace std;

//STUDENT//////////////////////////////////////////////////////////////////////////////////////

void Student::Add_mark(int mrk) {
		marks.push_back(mrk);
}

	double Student::av_mark() {
		vector<int>::const_iterator pos;
		int summ = 0;
		for (pos = marks.begin(); pos != marks.end(); ++pos)
		{
			summ += *pos;
		}
		return (double) summ / marks.size();
	}
	string Student::Mrk_str() {
		vector<int>::const_iterator pos;
		string temp("");
		char ch;
		int i = 0;
		for (pos = marks.begin(); pos != marks.end(); ++pos)
		{
			ch = *pos + '0';
			temp.append(1,ch);
		}
		return temp;
	}
	int Student::get_group() {
		
		return GR;
	}

	void Student::print_stud() {
		cout << ID << " " << FIO << " " << GR << " " << Mrk_str() <<" "<< av_mark() << endl;
	}

	void Student::Gr_add(int gr) {
		GR = gr;
	}

	string Student::Make_str() {
		return to_string(ID) +"," + FIO +","+ to_string(GR) +","+ Mrk_str();
	}

	//GROUP///////////////////////////////////////////////////////////////////////////////

	void Group::add_stud(Student* stud, int num) {
		Stud.push_back(stud);
		stud->Gr_add(num);
	}
	

	void Group::Head_elect() {
		
		Head = Stud[rand() % Stud.size()];
	}

		
	void Group::set_title(string nm) {
		Title = nm;
	}
	string Group::get_title() {
		return Title;
	}

	void Group::print_group() {
		vector<Student*>::const_iterator pos;
		cout <<"\n������ "<< get_title() << " " << Stud.size()<<"���."<<" "<<"������� ������: " << Av_mark() << endl;
		cout << "\n�������� " << endl;
		Head->print_stud();
		cout << endl;
		for (pos = Stud.begin(); pos != Stud.end(); ++pos)
		{
			if ((*pos)!=Head)
			(*pos)->print_stud();
		}
	}

	double Group::Av_mark() {
		vector<Student*>::const_iterator pos;
		double summ = 0;
		for (pos = Stud.begin(); pos != Stud.end(); ++pos)
		{
			summ += (*pos)->av_mark();
		}
		return summ / Stud.size();
	}

	void Group::remove_stud(int id) {
		vector<Student*>::const_iterator pos;
		for (pos = Stud.begin(); pos != Stud.end();pos++ )
			if ((*pos)->ID == id) {
				if ((*pos) == Head) {
					Stud.erase(pos);
					Head_elect();
				}
				else Stud.erase(pos);
				break;
			}
	}



	//DEKANAT//////////////////////////////////////////////////////////////////////////////////

	void Dekanat::group_form() {
		
		list<Student*>::iterator pos;
		for (int i = 0; i < 3; i++) {
			groups[i].set_title(group_title[i]);
			for (pos = names.begin(); pos != names.end(); ++pos) {
				if ((*pos)->get_group() == i)
					groups[i].add_stud(*pos, i);
			}
			groups[i].Head_elect();
		}
	}

	void Dekanat::group_form(int* hd) {

		list<Student*>::iterator pos;
		for (int i = 0; i < 3; i++) {
			groups[i].set_title(group_title[i]);
			for (pos = names.begin(); pos != names.end(); ++pos) {
				if ((*pos)->get_group() == i)
					groups[i].add_stud(*pos, i);
				if ((*pos)->ID == *(hd + i))
					groups[i].Head = *pos;
			}
			}
	}

	void Dekanat::printNames() {
		list<Student*>::const_iterator pos;
		for (pos = names.begin(); pos != names.end(); ++pos) {
			(*pos)->print_stud();
		}
	}

	void Dekanat::Set_marks() {

		list<Student*>::const_iterator pos;
		for (pos = names.begin(); pos != names.end(); ++pos) {
			
			for (int j = 0; j < 5; j++)
			
			(*pos)->marks.push_back(2 + rand() % 4);
		}
	}
		void Dekanat::print_Gr() {
			for (auto i = 0; i < 3; i++)
				groups[i].print_group();
		}

		void Dekanat::List_worse() {
			list<Student*>::const_iterator pos;
			for (pos = names.begin(); pos != names.end();pos++ ) 

				if ((*pos)->av_mark() < 3.0) 
					(*pos)->print_stud();
		}

		void Dekanat::Stud_search(string name) {
			int k = 0;
			list<Student*>::const_iterator pos;
			for (pos =names.begin(); pos != names.end(); ++pos)
				if ((*pos)->FIO.find(name) != string::npos) {
					cout << endl;
					(*pos)->print_stud();
					k++;
				}
			if (!k)
					cout << "�� ������!" << endl;
		}

		void Dekanat::Stud_search(int id) {
			int k = 0;
			list<Student*>::const_iterator pos;
			for (pos = names.begin(); pos != names.end(); ++pos)
				if ((*pos)->ID == id) {
					cout << endl;
					(*pos)->print_stud();
					k++;
				}
					if (!k)
			cout << "�� ������!" << endl;
		}

		void Dekanat::Move_stud(int id, int k) {

			for (auto pos = names.begin(); pos != names.end(); pos++)

				if ((*pos)->ID == id) {
					groups[(*pos)->GR].remove_stud((*pos)->ID);
					(*pos)->GR = k;
					groups[k].Stud.push_back(*pos);
				}
		}
	
		void Dekanat::Rem_worse() {
			
			for (auto pos = names.begin(); pos != names.end();)

				if ((*pos)->av_mark() < 3.0) {
					groups[(*pos)->GR].remove_stud((*pos)->ID);
					names.erase(pos);
					pos = names.begin();
				}
				else pos++;
		}

		void Dekanat::Save_dek(){
			ofstream file1("dek.txt"),file2("heads.txt");
			
			for (auto pos = names.begin(); pos != names.end(); pos++)
				file1<< (*pos)->Make_str()<< endl;
			
			for (auto i = 0; i < 3; i++)
				file2<<groups[i].Head->ID<<endl;
			file1.close();
			file2.close();
		}
		
