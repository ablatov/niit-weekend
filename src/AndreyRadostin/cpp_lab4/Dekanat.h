#ifndef _DEKANAT_H_
#define _DEKANAT_H_
#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include<list>
#include<ctime>
#include <sstream>

using namespace std;

class Dekanat;
class Student;

class Group {
private:
	string Title;
	vector <Student*>  Stud;
	Student* Head;
public:
	Group() :Title(""), Stud(0), Head(0) {}
	~Group() {}
	void set_title(string);
	void add_stud(Student* stud, int num);
	void Head_elect();
	void print_group();
	void remove_stud(int);
	friend class Student;
	friend class Dekanat;
		
	string get_title(); 
	double Av_mark();
};

class Student {
private:
	int ID;
	string FIO;
	int GR;
	vector <int> marks;

public:
	//����������� ��� �������� ��������
	Student(int num, string Name, int gr) {
		ID = num;
		FIO = Name;
		GR = gr;
	};
	//����������� ��� �������������� ��������
	Student(string stud_str) {
		vector<string> arr;
		string delim(",");
		size_t prev = 0;
		size_t next;
		size_t delta = delim.length();

		while ((next = stud_str.find(delim, prev)) != string::npos) {
			string tmp =stud_str.substr(prev, next - prev);
			arr.push_back(stud_str.substr(prev, next - prev));
			prev = next + delta;
		}
		string tmp = stud_str.substr(prev);
		arr.push_back(stud_str.substr(prev));
		int ID = atoi(arr[0].c_str());
		string FIO = arr[1];
		int GR = atoi(arr[2].c_str());
		while (atoi(arr[3].c_str())) {
			marks.push_back(atoi(arr[3].c_str()) % 10);
			arr[3].pop_back();
		}
		reverse(marks.begin(), marks.end());
	};

	friend class Group;
	friend class Dekanat;
	~Student() {}
	void Gr_add(int gr);
	int get_group();
	void Add_mark(int mrk);
	double av_mark();
	void print_stud();
	string Make_str();
	string Mrk_str();
};


class Dekanat
{
private:
	list <Student*> names; 
	Group groups[3];
	string group_title[3] = { "�����", "��������", "������" };

public:
	//����������� ��� �������� ��������
	Dekanat() {
		string s;
		ifstream file("students.txt");
		int i = 1;
		srand(static_cast<unsigned int>(clock()));

		while (getline(file, s)) {
			
			Student* temp = new Student(i++, s, rand()%3);
			names.push_back(temp);
			s.erase();
		}
	}
	//����������� ��� �������������� ��������
	Dekanat(const char* f1, const char* f2) {
		ifstream file1(f1), file2(f2);
		string s;
		int heads[3];
		
		for (auto i = 0; i < 3; i++)
			file1 >> heads[i];
		while (getline(file2, s)) {

			Student* temp = new Student(s);
			names.push_back(temp);
			s.erase();
		}
		group_form(heads);
	}
	friend class Group;
	friend class Student;
		~Dekanat() {};
		void group_form();
		void group_form(int*);
		void printNames();
		void Set_marks();
		void print_Gr();
		void List_worse();
		void Stud_search(string name);
		void Stud_search(int id);
		void Move_stud(int, int);
		void Rem_worse();
		void Save_dek();
		
};


#endif